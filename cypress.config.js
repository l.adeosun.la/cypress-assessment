const { defineConfig } = require("cypress");
const allureWriter = require('@shelex/cypress-allure-plugin/writer');
const { allureCypress } = require("allure-cypress/reporter");


module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      allureWriter(on, config);
      allureCypress(on);

    },
    env: {
      baseUrl: "https://www.saucedemo.com",
    },
    chromeWebSecurity: false 
  },
  // reporter: "./node_modules/@shelex/cypress-allure-plugin"
});

