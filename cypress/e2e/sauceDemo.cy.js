import LoginPage from '../pageObjects/loginPageObject';
import InventoryPage from '../pageObjects/inventoryPageObject';

describe('SauceDemo Item Sorting Validation', () => {
  beforeEach(() => {
    LoginPage.visitLoginPage();
  });

  it('should verify item sorting functionality', () => {
    cy.fixture("testdata").then((data) => {
      LoginPage.login(data.standardUser.username, data.standardUser.password);
      InventoryPage.verifyItemsSortedByNameAscending();
      InventoryPage.sortItemsByNameDescending();
      InventoryPage.verifyItemsSortedByNameDescending();
    });
  });
});


