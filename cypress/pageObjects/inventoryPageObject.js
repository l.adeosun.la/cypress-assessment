class InventoryPage {
  verifyItemsSortedByNameAscending() {
    cy.get('[data-test="product-sort-container"]').select("Name (A to Z)");
    cy.contains("Name (A to Z)").click({ force: true });
    cy.get(".inventory_item_name").then(($items) => {
      const itemNames = $items.toArray().map((item) => item.innerText);
      const sortedItemNames = [...itemNames].sort();
      expect(itemNames).to.deep.equal(sortedItemNames);
      cy.log("Successfully verified items sorted by name in ascending order");
    });
  }

  sortItemsByNameDescending() {
    cy.get('[data-test="product-sort-container"]').select("Name (Z to A)");
    cy.contains("Name (Z to A)").click({ force: true });
    cy.log("Sorted items by name in descending order");
  }

  verifyItemsSortedByNameDescending() {
    cy.get(".inventory_item_name").then(($items) => {
      const itemNames = $items.toArray().map((item) => item.innerText);
      const sortedItemNames = [...itemNames].sort().reverse();
      expect(itemNames).to.deep.equal(sortedItemNames);
      cy.log("Successfully verified items sorted by name in descending order");
    });
  }
}

module.exports = new InventoryPage();
