const baseUrl = Cypress.env("baseUrl");

class LoginPage {
  visitLoginPage() {
    if (!baseUrl) {
      throw new Error("Base URL is not defined, kindly check environment variables");
    }
    cy.visit(baseUrl);
  }

  login(username, password) {
    cy.get("#user-name").type(username);
    cy.get("#password").type(password);
    cy.get("#login-button").click();
  }
}

export default new LoginPage();
